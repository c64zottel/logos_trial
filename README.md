This is a proof of concept project. It parses a schema (still in development) for staterooms.
It essentially demonstrates the usage of the Rust crate 
[logos](https://crates.io/crates/logos).

The following schema is successfully parsed into tokens:

```rust
let mut schema = SchemaTokens::lexer(
r#"StateMachine = "StateMachine" VERSION ID "{" [description] 1*States "}"
    VERSION = ( 2DIGIT "." 2DIGIT )
    ID = 2*VCHAR
    description = *VCHAR
    States = "State:" ID "Transitions:" *Transition
    Transition = "Transition:" "{" ID "}"
"#
);
```

Tokens created:
```rust
#[derive(Logos, PartialEq)]
#[logos(skip r"[ \t\n\f]+")] // Ignore this regex pattern between tokens
pub enum SchemaTokens {
    #[regex("[[:alpha:]]+[[:alnum:]_]*", read_string)]
    Rule(String),

    #[regex(r#"[[:alpha:]][[:alnum:]_]+ ="#, read_rule_definition)]
    RuleDefinition(String),

    #[regex(r#"[[:digit:]]*\*?[[:digit:]]*"#, read_repeat)]
    RepeatItem((u32, u32)),

    #[regex(r#""[\x23-\x7E]+""#, read_quoted_string)]
    DoubleQuotedChars(String),

    #[regex(r"[[:digit:]]*\*?[[:digit:]]*DIGIT", read_DIGIT)]
    RepeatedDigit((u32, u32)),

    #[regex(r"[[:digit:]]*\*?[[:digit:]]*REF\([[:alnum:]_]*\)", read_REF)]
    RepeatedRef((u32, u32, String)),

    #[regex(r"[[:digit:]]*\*?[[:digit:]]*VCHAR", read_VCHAR)]
    RepeatedChars((u32, u32)),

    #[token("[")]
    OptionOpen,
    #[token("]")]
    OptionClose,
    #[token("(")]
    GroupOpen,
    #[token(")")]
    GroupClose,
    #[token("{")]
    CurlyOpen,
    #[token("}")]
    CurlyClose,
}

```