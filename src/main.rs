mod rcg_token;

use crate::rcg_token::SchemaTokens;
use logos::Logos;

fn get_rule_definition_slices(tokens: &Vec<SchemaTokens>) -> Vec<&[SchemaTokens]> {
    tokens
        .iter()
        .enumerate()
        .fold((Vec::new(), 0usize), |mut acc, e| {
            if matches!(e.1, &SchemaTokens::RuleDefinition(_)) {
                acc.0.push(&tokens[e.0..e.0]);
                acc.1 = e.0;
            } else {
                *acc.0.last_mut().unwrap() = &tokens[acc.1..e.0];
            }
            acc
        }).0
}

fn main() {

    let schema = SchemaTokens::lexer(
        r#"StateMachine = "StateMachine" Version ID [description] InitialTransition 1*States
        InitialTransition = "Transition" [ExecContent] "target:" 1*REF(state_id)
        States = "State" state_ID [OnEntry] [OnExit] *Transitions
        Transitions = "Transition" transition_ID ["target:" 1*REF(state_ID)] [Events]
        Events = "Events:" 1*event_ID

        ExecContent = VCHAR
        OnEntry = ExecContent
        OnExit = ExecContent
        ID = 2*VCHAR
        state_ID = ID
        event_ID = ID
        transition_ID = ID
        description = *VCHAR
        Version = ( 2*2DIGIT "." 2*2DIGIT )
        "#
    );
    let x = schema.map(|x| x.unwrap()).collect::<Vec<SchemaTokens>>();

    let indices = get_rule_definition_slices(&x);
    generate_code_definition(indices);

    let _lex = SchemaTokens::lexer(
        r#"StateMachine 00.01 UserInput
        State: S1
            Transition t1, target: S2
            Transition t2, target: S3
        State: S2
            Transition: t1, event: E2("hello"), target: S2
            Transition: t2, target: S3
        "#);

    let _parallel = SchemaTokens::lexer(
        r#"StateMachine 00.01 UserInput
        State S1
            Transition target: S2
            Transition target: S3
        State S2
            Transition event: E2("hello"), target: S2
            Transition target: S3
        State S3
            Transition event: E2("hello"), target: S2
            Transition target: S3
        "#);
}

fn generate_code_definition(rule_definitions: Vec<&[SchemaTokens]>) {
    for definition in rule_definitions {
        for rule in &*definition {
            dbg!(rule);
        }
    }
}

