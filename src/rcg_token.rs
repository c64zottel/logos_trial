use std::fmt::{Debug, Formatter};
use logos::{Lexer, Logos};

fn read_quoted_string(lex: &mut Lexer<SchemaTokens>) -> String {
    let s = lex.slice();
    dbg!(&s);
    s[1..s.len() - 1].to_string()
}

fn read_string(lex: &mut Lexer<SchemaTokens>) -> String {
    let s = lex.slice();
    dbg!(&s);
    s.to_string()
}

fn read_rule_definition(lex: &mut Lexer<SchemaTokens>) -> String {
    let s = lex.slice();
    dbg!(&s);
    s[..s.len() - 1].trim().to_string()
}


fn read_repeat(lex: &mut Lexer<SchemaTokens>) -> (u32, u32)
{
    let s = lex.slice();
    dbg!(&s);
    let mut n = s.split("*");
    // regex guarantees at least 1 element
    let n1 = n.next().unwrap().parse::<u32>().unwrap_or_default();
    let n2 = n.next();
    let n3 = n.next();
    let (x, y) = match n3 {
        None => {
            match n2 {
                None => (n1, n1),
                Some("") => (n1, u32::MAX),
                Some(value) => (n1, value.parse::<u32>().unwrap())
            }
        }
        Some(value) => (n1, value.parse::<u32>().unwrap())
    };

    (x, y)
}

#[allow(non_snake_case)]
fn read_DIGIT(lex: &mut Lexer<SchemaTokens>) -> (u32, u32)
{
    let s = lex.slice();
    let s = &s[..s.len() - "DIGIT".len()];
    dbg!(&s);
    let mut n = s.split("*");
    // regex guarantees at least 1 element
    let n1 = n.next().unwrap().parse::<u32>().unwrap_or_default();
    let n2 = n.next();
    let n3 = n.next();
    let (x, y) = match n3 {
        None => {
            match n2 {
                None => (n1, n1),
                Some("") => (n1, u32::MAX),
                Some(value) => (n1, value.parse::<u32>().unwrap())
            }
        }
        Some(value) => (n1, value.parse::<u32>().unwrap())
    };

    (x, y)
}

#[allow(non_snake_case)]
fn read_REF(lex: &mut Lexer<SchemaTokens>) -> (u32, u32, String)
{
    let s = lex.slice();
    let mut s = s.split("REF(").into_iter();
    let repeater = s.next().unwrap();
    let content = s.next().unwrap();
    dbg!(&repeater);
    let mut n = repeater.split("*");
    // regex guarantees at least 1 element
    let n1 = n.next().unwrap().parse::<u32>().unwrap_or_default();
    let n2 = n.next();
    let n3 = n.next();
    let (x, y) = match n3 {
        None => {
            match n2 {
                None => (n1, n1),
                Some("") => (n1, u32::MAX),
                Some(value) => (n1, value.parse::<u32>().unwrap())
            }
        }
        Some(value) => (n1, value.parse::<u32>().unwrap())
    };

    (x, y, content[..content.len()-1].to_string())
}

#[allow(non_snake_case)]
fn read_VCHAR(lex: &mut Lexer<SchemaTokens>) -> (u32, u32)
{
    let s = lex.slice();
    let s = &s[..s.len() - "VCHAR".len()];
    dbg!(&s);
    let mut n = s.split("*");
    // regex guarantees at least 1 element
    let n1 = n.next().unwrap().parse::<u32>().unwrap_or_default();
    let n2 = n.next();
    let n3 = n.next();
    let (x, y) = match n3 {
        None => {
            match n2 {
                None => (n1, n1),
                Some("") => (n1, u32::MAX),
                Some(value) => (n1, value.parse::<u32>().unwrap())
            }
        }
        Some(value) => (n1, value.parse::<u32>().unwrap())
    };

    dbg!(x, y);
    (x, y)
}


#[derive(Logos, PartialEq)]
#[logos(skip r"[ \t\n\f]+")] // Ignore this regex pattern between tokens
pub enum SchemaTokens {
    #[regex("[[:alpha:]]+[[:alnum:]_]*", read_string)]
    Rule(String),

    #[regex(r#"[[:alpha:]][[:alnum:]_]+ ="#, read_rule_definition)]
    RuleDefinition(String),

    #[regex(r#"[[:digit:]]*\*?[[:digit:]]*"#, read_repeat)]
    RepeatItem((u32, u32)),

    #[regex(r#""[\x23-\x7E]+""#, read_quoted_string)]
    DoubleQuotedChars(String),

    #[regex(r"[[:digit:]]*\*?[[:digit:]]*DIGIT", read_DIGIT)]
    RepeatedDigit((u32, u32)),

    #[regex(r"[[:digit:]]*\*?[[:digit:]]*REF\([[:alnum:]_]*\)", read_REF)]
    RepeatedRef((u32, u32, String)),

    #[regex(r"[[:digit:]]*\*?[[:digit:]]*VCHAR", read_VCHAR)]
    RepeatedChars((u32, u32)),

    #[token("[")]
    OptionOpen,
    #[token("]")]
    OptionClose,

    #[token("(")]
    GroupOpen,
    #[token(")")]
    GroupClose,

    #[token("{")]
    CurlyOpen,
    #[token("}")]
    CurlyClose,
}

impl Debug for SchemaTokens {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            SchemaTokens::Rule(value) => write!(f, "Rule: {}", value),
            SchemaTokens::RuleDefinition(value) => write!(f, "RuleDefinition: {}", value),
            SchemaTokens::RepeatItem(value) => write!(f, "Repeat: ({}, {})", value.0, value.1),
            SchemaTokens::DoubleQuotedChars(value) => write!(f, "DoubleQuotedChars: {}", value),
            SchemaTokens::OptionOpen => write!(f, "Option start"),
            SchemaTokens::OptionClose => write!(f, "Option end"),
            SchemaTokens::GroupOpen => write!(f, "group open"),
            SchemaTokens::GroupClose => write!(f, "group close"),
            SchemaTokens::CurlyOpen => write!(f, "curly open"),
            SchemaTokens::CurlyClose => write!(f, "curly close"),
            SchemaTokens::RepeatedDigit(value) => write!(f, "RepeatedDigit ({},{})", value.0, value.1),
            SchemaTokens::RepeatedRef(value) => write!(f, "RepeatedRef ({},{},{})", value.0, value.1, value.2),
            _ => write!(f, "unknow"),
        }
    }
}

#[test]
fn general_test() {
    let mut schema = SchemaTokens::lexer(
        r#"StateMachine = "StateMachine" VERSION ID "{" [description] 1*States "}"
        VERSION = ( 2DIGIT "." 2DIGIT )
        ID = 2*VCHAR
        description = *VCHAR
        States = "State:" ID "Transitions:" *Transition
        Transition = "Transition:" "{" ID "}"
        "#
    );

    assert_eq!(schema.next(), Some(Ok(SchemaTokens::RuleDefinition("StateMachine".to_string()))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::DoubleQuotedChars(String::from("StateMachine")))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::Rule(String::from("VERSION")))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::Rule(String::from("ID")))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::DoubleQuotedChars(String::from("{")))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::OptionOpen)));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::Rule(String::from("description")))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::OptionClose)));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::RepeatItem((1, 4294967295)))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::Rule(String::from("States")))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::DoubleQuotedChars(String::from("}")))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::RuleDefinition("VERSION".to_string()))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::GroupOpen)));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::RepeatedDigit((2, 2)))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::DoubleQuotedChars(String::from(".")))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::RepeatedDigit((2, 2)))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::GroupClose)));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::RuleDefinition("ID".to_string()))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::RepeatedChars((2, u32::MAX)))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::RuleDefinition("description".to_string()))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::RepeatedChars((0, u32::MAX)))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::RuleDefinition("States".to_string()))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::DoubleQuotedChars(String::from("State:")))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::Rule("ID".to_string()))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::DoubleQuotedChars("Transitions:".to_string()))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::RepeatItem((0, u32::MAX)))));
    assert_eq!(schema.next(), Some(Ok(SchemaTokens::Rule("Transition".to_string()))));
}